STYLE_FILE=/usr/share/yellowtire.xslt.xml
DATA_FILE=/tmp/revelation.xml

function a2ps ( )
{
    /usr/bin/a2ps "${@}"
}

function date ( )
{
    /bin/date "${@}"
}

function evince ( )
{
    /usr/bin/evince "${@}"
}

function mktemp ( )
{
    /bin/mktemp "${@}"
}

function rm ( )
{
    /bin/rm "${@}"
}

function xsltproc ( )
{
    /usr/bin/xsltproc "${@}"
}
