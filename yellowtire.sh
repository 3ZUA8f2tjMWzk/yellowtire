#!/bin/bash

unset PATH

echo ${#}

if [ 0 == ${#} ]
then
    source /etc/yellowtire.conf
else
    source "${@}"
fi

XML_FILE=$(mktemp)
TIMESTAMP=$(date +%s)
xsltproc --output "${XML_FILE}" --stringparam timestamp "${TIMESTAMP}" "${STYLE_FILE}" "${DATA_FILE}"
rm "${DATA_FILE}"
PS_FILE=$(mktemp)
a2ps -1 -o "${PS_FILE}" "${XML_FILE}"
rm -rf "${XML_FILE}"
evince "${PS_FILE}"
rm -rf "${PS_FILE}"

